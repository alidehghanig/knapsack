# Knapsack [![pipeline status](https://gitlab.com/alidehghanig/knapsack/badges/master/pipeline.svg)](https://gitlab.com/alidehghanig/knapsack/commits/master) [![coverage report](https://gitlab.com/alidehghanig/knapsack/badges/master/coverage.svg)](https://gitlab.com/alidehghanig/knapsack/commits/master)
An efficient implementation for 0-1 Knapsack problem using Dynamic Programming.

## Algorithms
We've used a pretty straightforward dynamic programming implementation of 0-1 Knapsack problem.

### Problem Definition
The more formal definition of the Knapsack problem is as following:
> We've got `n` items. The `ith`-item is worth `v`<sub>i</sub> and weighs `w`<sub>i</sub>, where `vs` and `ws` are
> decimal numbers. What we want is to take as valuable a load as possible while carrying at most `W` weight
> in our Knapsack (Hence the name).
> In the 0-1 Knapsack problem we must either take an item or leave it behind. On the contrary, in the
> fractional Knapsack problem, we can take a fractional amount of an item or take an item more than once.

### Dynamic Programming
Dynamic programming solves problems by breaking down the problem into a set of sub-problems recursively. After finding 
a solution for those sub-problems, it combines the sub-solutions to construct the solution for the actual problem.

Even though this definition is very similar with what Divide and Conquer does, these two approaches are different in 
very a subtle way. **Divide and Conquer works best when the the sub-problems are disjoint and have nothing in common. On 
the other hand, Dynamic Programming shines when the sub-problems overlap.** 

A dynamic programming algorithm solves each 
sub-problem once and save its result into a table. When the algorithm meets a previously solved sub-problem, instead of 
solving it for the second time, it just uses that saved value. Therefore, when the sub-problems overlap, Dynamic Programing 
can provide a huge performance boost.

#### Programming, Really?
Please note that the term *Programming* in *Dynamic Programming* refers to using a *tabular data structure* for storing 
the sub-solutions.

### Greedy Algorithm?
One might argue that the Dynamic Programming approach we've used here is a bit overkill. Instead proposes to use a Greedy
approach. As a matter of fact, to solve the problem, we could first compute the `v/w` for each item `i`. Obeying a greedy 
strategy, we begin by taking as much as possible of the item with the greatest `v/w`. If the supply of that item is exhausted 
and we can still carry more, we take as much as possible of the item with the next greatest `v/w`, and so forth, until 
reaching the weight limit `W`. Thus, by sorting the items by `v/w`, the greedy algorithm runs in `O(nlog)` time.

**To see that this greedy strategy does not work for the 0-1 knapsack problem**, consider the following problem:

![CLRS](https://gitlab.com/alidehghanig/knapsack/uploads/b799ea06e2d275ad90c306aa6a7d60a3/CLRS.png)

<sup>Courtesy of CLRS</sup>

This example has 3 items and a knapsack with weight limit of 50. Item 1 weighs 10 and is worth 60. Item 2 weighs 20 and 
is worth 100. Item 3 weighs 30 and is worth 120. Thus, the value per weight of item 1 is 6, which is greater than the 
value per weight of either item 2 (5) or item 3 (4). The greedy strategy, therefore, would take item 1 first. As you 
can see from the case analysis, however, the optimal solution takes items 2 and 3, leaving item 1 behind. The two possible 
solutions that take item 1 are both suboptimal.

### Implementation
Let's suppose the most valuable load weighs `W`. If we remove the `ith` item from this load, then the  remaining load 
should be the most valuable load weighing at most `W - w[i]` which does not include the removed item. Therefore, 
the solution for the 0-1 Knapsack problem contain an optimal sub-structure required by dynamic programming.

In order to formalize our solution, let's suppose we're going to decide to whether add the item `i` to a set of already 
optimized items. Here the term `table[i][j]` represents the maximum cost when choosing the first `i` items in a way that 
the total weight is less then `j`.

If the weight of `ith`-item is greater than the limit `j`, then we can't add this item to the group. So:
```
If w[i] > j then table[i][j] = table[i - 1][j]
```
Otherwise, we should compare the cost of adding and not adding this item and choose the one with greater
cost:
```
table[i][j] = max(table[i - 1][j], table[i - 1][j - w[i]] + cost[i])
```
#### Multiple Solutions
If two options are having the same cost, then we should choose the one with lower weight. In our bottom-up approach, 
if each `table[i][j]` guarantees that they always choose the lighter packages in case of identical costs, 
then we can be sure that the the `table[n][w]` (actual solution) also guarantees the same thing.

### Time & Space Complexities
Since Dynamic Programming algorithms are exploiting memory-time trade-offs, we have to analyze this
algorithm from both memory and time perspectives.

Before going any further, we should mention that the original 0-1 Knapsack problem was designed around
integral weights. Since all weights are decimal numbers here, we have to first multiply each weight by
a large enough power of ten to make all weights integral numbers.

The most memory consuming part of this algorithm is the memoized table. Since this is a two dimensional
array, our memory complexity is:
```
If we multiply each weight by 10 ^ d:
O(10 ^ d * nW)
For small enough d values, this is equivalent to:
O(nW)
```
    
The most time-consuming part of this algorithm is related to the nested for loops. So by the same logic,
our time complexity is also `O(nW)`.

### Caveats
Since in its traditional definition, the 0-1 Knapsack weight are integers, we have to first convert the decimal
weights to integral ones in order to use the classical solution. Here we assumed that the given weights have at
most 2 decimal places. Therefore we multiply all weights to 100 and cast them to integers. With this assumption,
the time and space complexity of the algorithm would be:
```
O(100nW) = O(nW)
```
Here we're going to stick with 2 decimal places but it's also possible to scan all weights and multiply the
weights with the maximum possible power of ten to avoid wrong answers due to lost precisions.

## Design Patterns & Best Practices
I've tried to incorporate design patterns and other best practices wherever they created a noticeable value. In the following
sections, I'm going talk about the motivation and mechanics of them.

### SOLID Principles
#### Single Responsibility
All abstractions I've used to trying to achieve one common goal and nothing more. For example:
 - The `Packer` class is responsible for reading and parsing the file.
 - The `ConstraintValidator` abstraction is responsible for abstracting away the constraint validation logic.
 - The `ConstraintValidators` composite class is responsible for applying all registered constraints. So the caller don't
 need to register validators and concern itself with the validation logic.
 - The `PackageOptimizer` abstraction is abstracting the Knapsack implementations.
 
#### Open-Closed
The whole library is open for extension without shutgun surgery. For example, in order to add a new constraint logic, just
introduce a new `ConstraintValidator` implementation and register it with `ConstraintValidators`.

### Strategy Interfaces
I've used strategy interfaces a few places. For example, we may need different implementations of Knapsack. So, we have
a `PackageOptimizer` interface with different implementations (Currently there is one implementation for 0-1 but we could
simply implement a Greedy based fractional Knapsack, too.).

Moreover, in order to support different constraint logic, there is `ConstraintValidator` logic with three different 
implementations.

### Composite Pattern
The `ConstraintValidators` class is a composite `ConstraintValidator` that know all possible implementations. Every time
we need to validate a constraint, we just need to call this class and it would take care of the rest. 

### Clean Code
The code is extremely clean: 
 - Documented everything as thorough as possible.
 - Incorporated a lot of well-named private methods to help you better understand the code. For example, here's how the 
 Knapsack implementation looks like:
 ```java
 if (items == null || items.isEmpty()) return Optional.empty();
 items = discardTooHeavyItems(items, maximumWeight); // O(n)
 
 int precision = 100; // 10 to the power of 2. That is, at most 2 decimal places.
 int[] weights = items.stream().mapToInt(i -> (int) (i.getWeight() * precision)).toArray(); // O(n)
 int w = (int) (maximumWeight * precision);
 int n = items.size();
 
 SubSolution[][] table = initializeMemoizedSolutions(n, w); // O(nW)
 for (int i = 1; i < n + 1; i++) {
     for (int j = 1; j < w + 1; j++) {
         solveAndMemoizeThisSubProblem(items, weights, table, i, j); // O(nW)
     }
 }
 
 return collectAndReturnTheAnswer(table[n][w]);
 ```

### TDD Mindset
Even though it's a bit hard to tell if a project was developed using TDD or not, I've used this approach while developing.

## Tests
In order to run tests and generate JaCoCo coverage results, just issue the following command:
```bash
> ./mvnw clean verify
```
Then open the `target/site/jacoco/index.html` to see the coverage report. Also, this report is available at our Gitlab
pages:

```
https://alidehghanig.gitlab.io/knapsack/cov/
```

## Built With
* [JUnit 5](https://junit.org/junit5/) - The testing framework on JVM.
* [JoCoCo](https://github.com/jacoco/jacoco) - As our *Code Coverage* tool.
* [Maven](https://maven.apache.org) - As our build tool.

and last but certainly not least:
* Java & JVM

## Contributing

Please read [CONTRIBUTING](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests.

## Versioning

We use [SemVer](http://semver.org/) for versioning.

## Authors

See also the list of [CONTRIBUTORS](CONTRIBUTORS.md) who participated in this project.

## References
When it comes to algorithms and data structures, there is no better reference than the famous `Introduction to Algorithms`
book by `Cormen, Leiserson, Rivest and Stein`.

## License
Copyright 2018 alidg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
