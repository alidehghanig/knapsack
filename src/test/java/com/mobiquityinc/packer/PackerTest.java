package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.exception.ConstraintViolationException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * Tests for the {@link Packer} class.
 *
 * @author Ali Dehghani
 */
class PackerTest {

    @Test
    @DisplayName("Given null filenames, should throw APIException with an appropriate message")
    void givenNullFileNameShouldThrowException() {
        assertThatThrownBy(() -> Packer.pack(null))
            .isInstanceOf(APIException.class)
            .hasMessage("The filename can't be null");
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "   ", "       "})
    @DisplayName("Given blank filenames, should throw APIException with an appropriate message")
    void givenBlankFileNameShouldThrowException(String blankPath) {
        assertThatThrownBy(() -> Packer.pack(blankPath))
            .isInstanceOf(APIException.class)
            .hasMessage("The filename can't be blank");
    }

    @ParameterizedTest
    @ValueSource(strings = {"/home/me/42.txt", "C:\\Users\\me\\42.txt"})
    @DisplayName("Given invalid filenames, should throw APIException with an appropriate message and cause")
    void givenInvalidFileNameShouldThrowException(String filePath) {
        assertThatThrownBy(() -> Packer.pack(filePath))
            .isInstanceOf(APIException.class)
            .hasCauseInstanceOf(IOException.class)
            .hasMessage("Failed to read from: " + filePath);
    }

    @Test
    @DisplayName("Each line should follow the 'weight : (details) (details)' format")
    void givenInvalidLineFormatShouldThrowException() {
        assertThatThrownBy(() -> Packer.pack(absolutePathOf("invalid-structure.txt")))
            .isInstanceOf(APIException.class)
            .hasMessage("Unexpected line format: malformed line");
    }

    @Test
    @DisplayName("Weight limit should be valid decimal number")
    void givenInvalidWeightShouldThrowException() {
        assertThatThrownBy(() -> Packer.pack(absolutePathOf("invalid-weight.txt")))
            .isInstanceOf(APIException.class)
            .hasCauseInstanceOf(NumberFormatException.class)
            .hasMessage("Weight limit isn't a number: 8.12a");
    }

    @Test
    @DisplayName("Item data are triplets like (index,weight,cost)")
    void givenInvalidItemDataFormatShouldThrowException() {
        assertThatThrownBy(() -> Packer.pack(absolutePathOf("invalid-item-format.txt")))
            .isInstanceOf(APIException.class)
            .hasMessage("Unexpected items format: 12,13");
    }

    @Test
    @DisplayName("Item index should be a valid integer")
    void givenInvalidItemIndexShouldThrowException() {
        assertThatThrownBy(() -> Packer.pack(absolutePathOf("invalid-index.txt")))
            .isInstanceOf(APIException.class)
            .hasMessage("Item data should be a valid number: 2a,88.62,€98");
    }

    @Test
    @DisplayName("Item weight should be a valid decimal")
    void givenInvalidItemWeightShouldThrowException() {
        assertThatThrownBy(() -> Packer.pack(absolutePathOf("invalid-item-weight.txt")))
            .isInstanceOf(APIException.class)
            .hasMessage("Item data should be a valid number: 2,b88.62,€98");
    }

    @Test
    @DisplayName("Item cost should be a valid decimal")
    void givenInvalidItemCostShouldThrowException() {
        assertThatThrownBy(() -> Packer.pack(absolutePathOf("invalid-cost.txt")))
            .isInstanceOf(APIException.class)
            .hasMessage("Item data should be a valid number: 2,88.62, €98.a");
    }

    @Test
    @DisplayName("First character of the item cost is a monetary sign")
    void givenInvalidItemCostSignShouldThrowException() {
        assertThatThrownBy(() -> Packer.pack(absolutePathOf("invalid-cost-sign.txt")))
            .isInstanceOf(APIException.class)
            .hasMessage("Item cost should be in valid format: ");
    }

    @Test
    @DisplayName("A problem should not surpass the max weight limit")
    void givenALargeMaxWeightShouldThrowAnException() {
        assertThatThrownBy(() -> Packer.pack(absolutePathOf("max-global-weight-limit.txt")))
            .isInstanceOf(ConstraintViolationException.class)
            .hasMessage("Surpassed the max weight limit: 175.0");
    }

    @Test
    @DisplayName("A Problem should not contain too much items to pick from")
    void givenTooMuchItemsInOneProblemShouldThrownAnException() {
        assertThatThrownBy(() -> Packer.pack(absolutePathOf("too-much-items.txt")))
            .isInstanceOf(ConstraintViolationException.class)
            .hasMessage("Exceeding the maximum number of items: 16");
    }

    @Test
    @DisplayName("An item should not surpass max weight limit")
    void givenHeavyItemsShouldThrownAnException() {
        assertThatThrownBy(() -> Packer.pack(absolutePathOf("max-weight-per-item.txt")))
            .isInstanceOf(ConstraintViolationException.class)
            .hasMessage("The item '1' has surpassed the maximum weight: 185.31");
    }

    @Test
    @DisplayName("An item should not surpass max cost limit")
    void givenTooValuableItemsShouldThrownAnException() {
        assertThatThrownBy(() -> Packer.pack(absolutePathOf("max-cost-per-item.txt")))
            .isInstanceOf(ConstraintViolationException.class)
            .hasMessage("The item '3' has surpassed the maximum cost: 116.0");
    }

    @Test
    @DisplayName("Given valid input should be able to solve each line and concat the results")
    void givenValidInputItShouldBeAbleToSolveEachLine() {
        String result = Packer.pack(absolutePathOf("valid-input.txt"));
        assertThat(result).isEqualTo("4\n-\n-\n-\n2,7\n8,9");
    }

    private String absolutePathOf(String classpathResource) {
        try {
            return Paths.get(ClassLoader.getSystemResource(classpathResource).toURI()).toString();
        } catch (URISyntaxException e) {
            throw new RuntimeException("Failed to find the file: " + classpathResource);
        }
    }
}
