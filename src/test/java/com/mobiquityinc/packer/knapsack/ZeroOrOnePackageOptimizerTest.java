package com.mobiquityinc.packer.knapsack;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;
import java.util.stream.Stream;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

/**
 * Unit tests for the {@link ZeroOrOnePackageOptimizer} class.
 *
 * @author Ali Dehghani
 */
class ZeroOrOnePackageOptimizerTest {

    /**
     * Subject under test.
     */
    private final PackageOptimizer optimizer = ZeroOrOnePackageOptimizer.INSTANCE;

    @Test
    @DisplayName("When the items input is null or an empty collection, should return an empty collection")
    void givenNullOrEmptyInputShouldReturnEmptyOptional() {
        assertThat(optimizer.optimize(null, 100)).isEmpty();
        assertThat(optimizer.optimize(new ArrayList<>(), 100)).isEmpty();
    }

    @ParameterizedTest
    @SuppressWarnings("OptionalGetWithoutIsPresent")
    @MethodSource("provideDifferentItemsAndWeightCombinations")
    @DisplayName("Given different combinations of items and max weight, should choose wisely")
    void givenItemsAndWeightShouldChooseWisely(List<Item> items, double maxWeight, int[] expectedIndices) {
        Optional<SelectedItems> result = optimizer.optimize(items, maxWeight);

        int[] actualChosenIndices = result.get().getItems().keySet().stream().mapToInt(Item::getIndex).toArray();
        assertThat(actualChosenIndices).containsExactly(expectedIndices);
    }

    @ParameterizedTest
    @MethodSource("provideItemsWithNoPossibleSelection")
    @DisplayName("When we can't select any of the given items, should return empty optional")
    void canNotSelectAnyItemShouldReturnEmptyOptional(List<Item> items, double maxWeight) {
        Optional<SelectedItems> result = optimizer.optimize(items, maxWeight);
        assertThat(result).isEmpty();
    }

    private static Stream<Arguments> provideDifferentItemsAndWeightCombinations() {
        List<Item> firstSet = Arrays.asList(
            new Item(1, 53.38, 45), new Item(2, 88.62, 98), new Item(3, 78.48, 3), new Item(4, 72.30, 76),
            new Item(5, 30.18, 9), new Item(6, 46.34, 48)
        );
        List<Item> secondSet = Arrays.asList(
            new Item(1, 85.31, 29), new Item(2, 14.55, 74), new Item(3, 3.98, 16), new Item(4, 26.24, 55),
            new Item(5, 63.69, 52), new Item(6, 76.25, 75), new Item(7, 60.02, 74), new Item(8, 93.18, 35),
            new Item(9, 89.95, 78)
        );
        List<Item> thirdSet = Arrays.asList(
            new Item(1, 90.72, 13), new Item(2, 33.80, 40), new Item(3, 43.15, 10), new Item(4, 37.97, 16),
            new Item(5, 46.81, 36), new Item(6, 48.77, 79), new Item(7, 81.80, 45), new Item(8, 19.36, 79),
            new Item(9, 6.76, 64)
        );

        // Greedy approach (i.e. selecting based on descending cost/weight ratios) fails to solve this
        // simple dataset.
        List<Item> fourthSet = Arrays.asList(
            new Item(1, 1, 6), new Item(2, 2, 10), new Item(3, 3, 12)
        );

        List<Item> fifthSet = Arrays.asList(
            new Item(1, 2, 3), new Item(2, 1, 2), new Item(3, 3, 3)
        );

        List<Item> largeDataset = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 1_000_000; i++) {
            largeDataset.add(new Item(i, random.nextInt(500) + 10, 1000));
        }
        largeDataset.addAll(fifthSet);

        return Stream.of(
            // 81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) 6,46.34,€48) => 4
            arguments(firstSet, 81.0, new int[]{4}),

            // 75 : (1,85.31,€29) (2,14.55,€74) (3,3.98,€16) (4,26.24,€55) (5,63.69,€52) (6,76.25,€75)
            // (7,60.02,€74) (8,93.18,€35) (9,89.95,€78) => 2,7
            arguments(secondSet, 75, new int[]{2, 7}),

            // 56 : (1,90.72,€13) (2,33.80,€40) (3,43.15,€10) (4,37.97,€16) (5,46.81,€36) (6,48.77,€79)
            // (7,81.80,€45) (8,19.36,€79) (9,6.76,€64) => 8, 9
            arguments(thirdSet, 56, new int[]{8, 9}),

            arguments(fourthSet, 5, new int[]{2, 3}),
            arguments(fifthSet, 5, new int[]{1, 3}),
            arguments(largeDataset, 5, new int[]{1, 3})
        );
    }

    private static Stream<Arguments> provideItemsWithNoPossibleSelection() {
        List<Item> largeDataset = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 1_000_000; i++) {
            largeDataset.add(new Item(i, random.nextInt(500) + 10, 1000));
        }

        return Stream.of(
            // 8 : (1,15.3,€34) => -
            arguments(singletonList(new Item(1, 15, 34)), 8),
            arguments(largeDataset, 5)
        );
    }
}
