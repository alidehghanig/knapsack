package com.mobiquityinc.packer.knapsack;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.Collections.singletonMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.params.provider.Arguments.arguments;

/**
 * Unit tests for {@link SelectedItems} class.
 *
 * @author Ali Dehghani
 */
class SelectedItemsTest {

    @Test
    @DisplayName("The mapping between selected items and their corresponding fraction should be present")
    void selectedItemsCanNotBeNull() {
        assertThatThrownBy(() -> new SelectedItems(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("The mapping between items and their corresponding fraction can't be null");
    }

    @ParameterizedTest
    @MethodSource("selectDifferentSetOfItems")
    @DisplayName("Total weight and cost should be pre-computed as expected")
    void totalWeightAndCostShouldBePreComputedAsExpected(Map<Item, Double> items, double expectedWeight, double expectedCost) {
        SelectedItems selectedItems = new SelectedItems(items);

        assertThat(selectedItems.getTotalCost()).isEqualTo(expectedCost);
        assertThat(selectedItems.getTotalWeight()).isEqualTo(expectedWeight);
        assertThat(selectedItems.getItems()).isEqualTo(items);
    }

    private static Stream<Arguments> selectDifferentSetOfItems() {
        Item first = new Item(1, 10, 100);
        Item sec = new Item(2, 12, 12);

        return Stream.of(
            arguments(singletonMap(first, 1.0), 10, 100),
            arguments(singletonMap(first, 2.0), 20, 200),
            arguments(items(first, 0.5, sec, 1.0), 17, 62),
            arguments(items(first, 2.0, sec, 2.0), 44, 224)
        );
    }

    private static Map<Item, Double> items(Item first, double firstFraction, Item sec, double secFraction) {
        Map<Item, Double> map = new HashMap<>();
        map.put(first, firstFraction);
        map.put(sec, secFraction);

        return map;
    }
}
