package com.mobiquityinc.packer.knapsack;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

/**
 * Unit tests for the {@link Item} class.
 *
 * @author Ali Dehghani
 */
class ItemTest {

    @ParameterizedTest
    @MethodSource("provideDifferentItems")
    @DisplayName("Items are equal iff they share the same index")
    void itemsAreEqualIffTheyShareTheSameIndex(Item first, Object second, boolean areEqual) {
        assertThat(first.equals(second)).isEqualTo(areEqual);
        if (areEqual) assertThat(first.hashCode()).isEqualTo(second.hashCode());
    }

    private static Stream<Arguments> provideDifferentItems() {
        Item item = new Item(1, 0, 0);

        return Stream.of(
            arguments(item, item, true),
            arguments(item, new Item(1, 1, 1), true),
            arguments(item, new Item(1, 0, 0), true),
            arguments(item, null, false),
            arguments(item, new Object(), false),
            arguments(item, new Item(2, 0, 0), false)
        );
    }
}
