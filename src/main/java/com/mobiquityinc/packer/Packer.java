package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.constraint.ConstraintValidators;
import com.mobiquityinc.packer.knapsack.Item;
import com.mobiquityinc.packer.knapsack.SelectedItems;
import com.mobiquityinc.packer.knapsack.ZeroOrOnePackageOptimizer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.joining;

/**
 * Reads a few lines from the given filepath and tries to solve packing problems in each line.
 *
 * @author Ali Dehghani
 */
public class Packer {

    /**
     * A pattern to extract the item data with the following format:
     * <pre>
     *     (anything) (anything) ...
     * </pre>
     */
    private static final Pattern ITEMS_PATTERN = Pattern.compile("\\((.*?)\\)");

    /**
     * Don't instantiate me!
     */
    private Packer() {
    }

    /**
     * Reads each line from the given file, extracts the Knapsack problem inputs from them
     * and solves each problem.
     *
     * @param filePath The path to the file.
     * @return A comma-separate collection of answers in each line. The '-' means there are
     * no answers for this particular problem.
     * @throws APIException When the given input is invalid.
     * @throws com.mobiquityinc.exception.ConstraintViolationException When one of the system constrains
     * has been violated.
     */
    public static String pack(String filePath) {
        if (filePath == null) throw new APIException("The filename can't be null");
        if (filePath.trim().isEmpty()) throw new APIException("The filename can't be blank");

        try {
            return Files.lines(Paths.get(filePath))
                .filter(line -> !line.trim().isEmpty())
                .map(Packer::extractItemsFromEachLine)
                .filter(i -> {
                    ConstraintValidators.INSTANCE.validate(i.items, i.weightLimit);
                    return true;
                })
                .map(i -> ZeroOrOnePackageOptimizer.INSTANCE.optimize(i.items, i.weightLimit))
                .map(result -> result.map(Packer::joinByComma).orElse("-"))
                .collect(joining("\n"));
        } catch (IOException e) {
            throw new APIException("Failed to read from: " + filePath, e);
        }
    }

    /**
     * Given a line, it parses the weight limit and item information and returns the parsed
     * result. The line should follow this format:
     * <pre>
     *     <weight_limit_number> : (<index>,<weight>,$<cost>)*
     * </pre>
     *
     * @param line The line describing a problem.
     * @return The weight limit and items information.
     * @throws APIException When the given line does not follow the expected format.
     */
    private static Items extractItemsFromEachLine(String line) {
        String[] parts = line.trim().split(":", 2);
        if (parts.length != 2) throw new APIException("Unexpected line format: " + line);

        double weightLimit = extractWeightLimit(parts[0]);
        List<Item> items = extractItems(parts[1]);

        return new Items(items, weightLimit);
    }

    /**
     * Parses the weight limit from the beginning of the line.
     *
     * @param possibleNumber The string representation of the weight limit.
     * @return The parsed number.
     * @throws APIException When the given string is not a valid number.
     */
    private static double extractWeightLimit(String possibleNumber) {
        try {
            return Double.parseDouble(possibleNumber.trim());
        } catch (NumberFormatException e) {
            throw new APIException("Weight limit isn't a number: " + possibleNumber, e);
        }
    }

    /**
     * Extracts all items information from the given input. It expects each item information to be
     * enclosed inside a pair of parentheses:
     * <pre>
     *     (<index>,<weight>,$<cost>)*
     * </pre>
     *
     * @param itemPart Describes a set of items.
     * @return Collection of items.
     * @throws APIException When the given input does not follow the expected format.
     */
    private static List<Item> extractItems(String itemPart) {
        List<Item> items = new ArrayList<>();
        Matcher matcher = ITEMS_PATTERN.matcher(itemPart);

        while (matcher.find()) {
            String insideParentheses = matcher.group(1);
            String[] itemData = insideParentheses.split(",", 3);
            if (itemData.length != 3) throw new APIException("Unexpected items format: " + insideParentheses);

            try {
                int index = Integer.parseInt(itemData[0].trim());
                double weight = Double.parseDouble(itemData[1].trim());
                double cost = Double.parseDouble(itemData[2].trim().substring(1));

                items.add(new Item(index, weight, cost));
            } catch (NumberFormatException e) {
                throw new APIException("Item data should be a valid number: " + insideParentheses, e);
            } catch (IndexOutOfBoundsException e) {
                throw new APIException("Item cost should be in valid format: " + itemData[2], e);
            }
        }

        return items;
    }

    /**
     * Converts the result of the optimizer to a comma-separated list of item indices.
     *
     * @param items Collection of selected items.
     * @return comma-separated collection of item indices.
     */
    private static String joinByComma(SelectedItems items) {
        return items.getItems().keySet().stream().map(i -> i.getIndex() + "").collect(joining(","));
    }

    /**
     * Utility abstraction to encapsulate weight limit and item information.
     */
    private static class Items {

        /**
         * Collection of items to choose from.
         */
        private final List<Item> items;

        /**
         * Weight limit for the current problem.
         */
        private final double weightLimit;

        public Items(List<Item> items, double weightLimit) {
            this.items = items;
            this.weightLimit = weightLimit;
        }
    }
}
