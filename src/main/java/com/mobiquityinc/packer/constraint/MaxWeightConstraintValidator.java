package com.mobiquityinc.packer.constraint;

import com.mobiquityinc.exception.ConstraintViolationException;
import com.mobiquityinc.packer.knapsack.Item;

import java.util.List;

/**
 * A simple {@link ConstraintValidator} implementation that simply enforce an upper bound
 * on the weight limit.
 *
 * @author Ali Dehghani
 */
final class MaxWeightConstraintValidator implements ConstraintValidator {

    /**
     * Max weight that a package can take should be less than or equal to 100.
     */
    private static final double MAX_WEIGHT = 100;

    /**
     * If the given weight limit surpasses the {@link #MAX_WEIGHT}, then throws a violation
     * exception.
     *
     * @param items       The collection of items.
     * @param weightLimit The weight limit.
     * @throws ConstraintViolationException When the given weight limit surpasses the maximum possible
     *                                      one.
     */
    @Override
    public void validate(List<Item> items, double weightLimit) {
        if (weightLimit > MAX_WEIGHT) {
            throw new ConstraintViolationException("Surpassed the max weight limit: " + weightLimit);
        }
    }
}
