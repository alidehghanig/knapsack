package com.mobiquityinc.packer.constraint;

import com.mobiquityinc.packer.knapsack.Item;

import java.util.Arrays;
import java.util.List;

/**
 * A composite singleton {@link ConstraintValidator} implementation that knows all other implementations
 * of the {@link ConstraintValidator}. When calling the {@link #validate(List, double)} it simply
 * loops through all known implementations and calls their {@link #validate(List, double)} method.
 *
 * @author Ali Dehghani
 */
public enum ConstraintValidators implements ConstraintValidator {

    /**
     * The only instance of this composite implementation.
     */
    INSTANCE;

    /**
     * All known implementations.
     */
    private final List<ConstraintValidator> validators = Arrays.asList(
        new MaxWeightConstraintValidator(),
        new ItemsCountConstraintValidator(),
        new PerItemConstraintValidator()
    );

    /**
     * Iterates through all known implementations and delegates the validation to them. If one of those
     * known implementations throw an exception, it re-throws it and terminates.
     *
     * @param items       The collection of items.
     * @param weightLimit The weight limit.
     * @throws com.mobiquityinc.exception.ConstraintViolationException If one of the known implementations do so.
     */
    @Override
    public void validate(List<Item> items, double weightLimit) {
        validators.forEach(v -> v.validate(items, weightLimit));
    }
}
