package com.mobiquityinc.packer.constraint;

import com.mobiquityinc.exception.ConstraintViolationException;
import com.mobiquityinc.packer.knapsack.Item;

import java.util.List;

/**
 * Makes sure that there only a limited number of items to choose from.
 *
 * @author Ali Dehghani
 */
final class ItemsCountConstraintValidator implements ConstraintValidator {

    /**
     * There might be up to 15 items you need to choose from.
     */
    private static final int MAX_ITEMS_COUNT = 15;

    /**
     * Makes sure that at any given problem, we have at most {@link #MAX_ITEMS_COUNT} items
     * to process. Otherwise, throws a {@link ConstraintViolationException}.
     *
     * @param items       The collection of items.
     * @param weightLimit The weight limit.
     * @throws ConstraintViolationException When there are too much items to process.
     */
    @Override
    public void validate(List<Item> items, double weightLimit) {
        if (items.size() > MAX_ITEMS_COUNT) {
            throw new ConstraintViolationException("Exceeding the maximum number of items: " + items.size());
        }
    }
}
