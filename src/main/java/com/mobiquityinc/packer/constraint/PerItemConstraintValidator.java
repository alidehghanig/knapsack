package com.mobiquityinc.packer.constraint;

import com.mobiquityinc.exception.ConstraintViolationException;
import com.mobiquityinc.packer.knapsack.Item;

import java.util.List;

/**
 * A {@link ConstraintValidator} to enforce some limitations on per item basis.
 *
 * @author Ali Dehghani
 */
final class PerItemConstraintValidator implements ConstraintValidator {

    /**
     * Maximum weight of an item should be less than or equal to 100.
     */
    private static final double MAX_WEIGHT = 100;

    /**
     * Maximum cost of an item should be less than or equal to 100.
     */
    private static final double MAX_COST = 100;

    /**
     * Makes sure that the max weight of each item does not exceed the {@link #MAX_WEIGHT} and
     * the maximum cost of each one does not surpass the {@link #MAX_COST}. Throws an exception
     * otherwise.
     *
     * @param items       The collection of items.
     * @param weightLimit The weight limit.
     * @throws ConstraintViolationException When an item violates the max weight or max cost limitations.
     */
    @Override
    public void validate(List<Item> items, double weightLimit) {
        items.forEach(item -> {
            if (item.getWeight() > MAX_WEIGHT) {
                throw new ConstraintViolationException("The item '" + item.getIndex() +
                    "' has surpassed the maximum weight: " + item.getWeight());
            }

            if (item.getCost() > MAX_COST) {
                throw new ConstraintViolationException("The item '" + item.getIndex() +
                    "' has surpassed the maximum cost: " + item.getCost());
            }
        });
    }
}
