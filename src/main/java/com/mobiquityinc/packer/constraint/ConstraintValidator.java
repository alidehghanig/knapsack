package com.mobiquityinc.packer.constraint;

import com.mobiquityinc.packer.knapsack.Item;

import java.util.List;

/**
 * Abstracts the constraint validation and enforcement process.
 *
 * @author Ali Dehghani
 * @apiNote The interface and all non-composite implementations are deliberately package-local. The
 * ony way to access the constraint validation logic is through the {@link ConstraintValidators}
 * composite implementation.
 * @see ConstraintValidators
 */
interface ConstraintValidator {

    /**
     * Given a set of parameters, it should evaluate that the given state is valid or
     * not. If it's not valid, then an exception of type {@link com.mobiquityinc.exception.ConstraintViolationException}
     * should be thrown.
     *
     * @param items       The collection of items.
     * @param weightLimit The weight limit.
     * @throws com.mobiquityinc.exception.ConstraintViolationException When the given state is not valid.
     */
    void validate(List<Item> items, double weightLimit);
}
