package com.mobiquityinc.packer.knapsack;

import java.util.Objects;

/**
 * An immutable data structure encapsulating each item details in the
 * Knapsack (i.e. packaging) problem. This data structure can be used
 * for both 0-1 and fractional use cases.
 *
 * @author Ali Dehghani
 */
public final class Item {

    /**
     * Represents the unique index of this particular item.
     */
    private final int index;

    /**
     * Represents the weight of this particular item. It also accepts negative
     * values as some applications of the algorithm may need to work with negative
     * weights.
     */
    private final double weight;

    /**
     * Represents the cost of this particular item. It also accepts negative
     * values as some applications of the algorithm may need to work with negative
     * costs or priorities.
     */
    private final double cost;

    /**
     * Constructs a new item instance.
     *
     * @param index  Item index.
     * @param weight Item's weight.
     * @param cost   Item's cost.
     */
    public Item(int index, double weight, double cost) {
        this.index = index;
        this.weight = weight;
        this.cost = cost;
    }

    /**
     * @return {@link #index}
     */
    public int getIndex() {
        return index;
    }

    /**
     * @return {@link #weight}
     */
    public double getWeight() {
        return weight;
    }

    /**
     * @return {@link #cost}
     */
    public double getCost() {
        return cost;
    }

    /**
     * Two items are equal iff they share the same {@link #index} value.
     *
     * @param other The other item to compare.
     * @return true if both items are having the same index. Returns false otherwise.
     */
    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;

        if (other == null || getClass() != other.getClass())
            return false;

        Item item = (Item) other;
        return index == item.index;
    }

    /**
     * @return An {@link #equals(Object)} compatible hashcode.
     */
    @Override
    public int hashCode() {
        return Objects.hash(index);
    }

    /**
     * @return A simple string representation of the
     */
    @Override
    public String toString() {
        return "Item{" + "index=" + index + ", weight=" + weight + ", cost=" + cost + '}';
    }
}
