package com.mobiquityinc.packer.knapsack;

import java.util.Map;

import static java.util.Collections.unmodifiableMap;
import static java.util.Objects.requireNonNull;

/**
 * An immutable collection of selected items along with a few metadata about the selection.
 *
 * @author Ali Dehghani
 */
public final class SelectedItems {

    /**
     * Represents the collection of selected {@link Item}s. Also, we map each item to a decimal number.
     * This decimal number represents the fractional amount of that item we picked. More precisely, for
     * 0-1 implementations this value is always 1 and for fractional implementations, this value is equal
     * to the actual fraction for that item.
     */
    private final Map<Item, Double> items;

    /**
     * Represents the total weight the current selection. Although this weight can be computed from the
     * {@link #items}, it wouldn't hurt to have this precomputed value.
     */
    private final double totalWeight;

    /**
     * Represents the total cost the current selection. Although this cost can be computed from the
     * {@link #items}, it wouldn't hurt to have this precomputed value.
     */
    private final double totalCost;

    /**
     * Constructs a new collection of selected items.
     *
     * @param items The non-null selected items.
     * @throws NullPointerException When the given map of items is null.
     */
    public SelectedItems(Map<Item, Double> items) {
        requireNonNull(items, "The mapping between items and their corresponding fraction can't be null");

        this.items = unmodifiableMap(items);
        this.totalWeight = items.entrySet().parallelStream().mapToDouble(e -> e.getKey().getWeight() * e.getValue()).sum();
        this.totalCost = items.entrySet().parallelStream().mapToDouble(e -> e.getKey().getCost() * e.getValue()).sum();
    }

    /**
     * @return {@link #items}
     */
    public Map<Item, Double> getItems() {
        return items;
    }

    /**
     * @return {@link #totalWeight}
     */
    public double getTotalWeight() {
        return totalWeight;
    }

    /**
     * @return {@link #totalCost}
     */
    public double getTotalCost() {
        return totalCost;
    }
}
