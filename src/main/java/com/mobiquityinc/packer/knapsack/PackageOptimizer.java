package com.mobiquityinc.packer.knapsack;

import java.util.List;
import java.util.Optional;

/**
 * A simple contract for different Knapsack implementations. Basically, given a collection of
 * {@link Item}s, we're going pick a few of them in way that:
 * <ul>
 *     <li>The total weight of all selected items is less than or equal to a maximum weight.</li>
 *     <li>The total cost is the maximum possible.</li>
 * </ul>
 * Even though it's possible to pick one item both entirely or partially, it's totally up the implementor's
 * discretion. That is, we may have different implementations for 0-1 Knapsack and Fractional Knapsack
 * algorithms.
 *
 * <h2>Problem Definition</h2>
 * The more formal definition of the Knapsack problem is as following:
 * <pre>
 *     We've got n items. The ith-item is worth v<sub>i</sub> and weighs w<sub>i</sub>, where vs and ws are
 *     decimal numbers. What we want is to take as valuable a load as possible while carrying at most W weight
 *     in our Knapsack (Hence the name).
 *     In the 0-1 Knapsack problem we must either take an item or leave it behind. On the contrary, in the
 *     fractional Knapsack problem, we can take a fractional amount of an item or take an item more than once.
 * </pre>
 *
 * <h2>Selected Items</h2>
 * The implementation should return the less heavier combinations when multiple combination of items achieve
 * the maximum cost.
 *
 * <h2>On Naming</h2>
 * For lack of a better name, we're going to call this interface {@code PackageOptimizer} instead
 * of Knapsack, as the former may better describe what we're trying to achieve.
 *
 * @author Ali Dehghani
 */
public interface PackageOptimizer {

    /**
     * Given a collection of items, we're going a pick a few of them in a way that we maximize the total
     * cost while the total weight is less than or equal to {@code maximumWeight}. When we can't pick any
     * item or there is nothing to pick, we should return an empty {@link Optional} value.
     * Moreover, the implementations should never throw exceptions and only communicate their result with
     * the actual return values.
     *
     * @param items         Collection of items to pick from.
     * @param maximumWeight The maximum possible weight that the Knapsack can hold.
     * @return Possible collection of items along with a few metadata about the selection. Returns an empty
     * {@link Optional} when there is nothing to pick.
     */
    Optional<SelectedItems> optimize(List<Item> items, double maximumWeight);
}
