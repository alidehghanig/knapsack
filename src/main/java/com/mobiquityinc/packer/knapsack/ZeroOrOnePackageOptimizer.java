package com.mobiquityinc.packer.knapsack;

import java.util.*;

import static java.util.stream.Collectors.toMap;

/**
 * A 0-1 Knapsack Dynamic Programming based implementation of {@link PackageOptimizer}.
 *
 * <h2>What is Dynamic Programming?</h2>
 * Dynamic programming solves problems by breaking down the problem into a set of sub-problems
 * recursively. After finding a solution for those sub-problems, it combines the sub-solutions
 * to construct the solution for the actual problem.
 * Even though this definition is very similar with what Divide and Conquer does, these two
 * approaches are different in very a subtle way. Divide and Conquer works best when the the
 * sub-problems are disjoint and have nothing in common. On the other hand, Dynamic Programming
 * shines when the sub-problems overlap. A dynamic programming algorithm solves each sub-problem
 * once and save its result into a table. When the algorithm meets a previously solved sub-problem,
 * instead of solving it for the second time, it just uses that saved value. Therefore, when the
 * sub-problems overlap, Dynamic Programing can provide a huge performance boost.
 * <p>
 * Please note that the term "programming" in "Dynamic Programming" refers to using a tabular data
 * structure for storing the sub-solutions.
 *
 * <h2>How this Works?</h2>
 * Let's suppose the most valuable load weighs {@code W}. If we remove the {@code ith} item from this
 * load, then the remaining load should be the most valuable load weighing at most {@code W - w[i]}
 * which does not include the removed item. Therefore, the solution for the 0-1 Knapsack problem contains
 * an optimal sub-structure required by dynamic programming.
 * <p>In order to formalize our solution, let's suppose we're going to decide to whether add the item {@code i}
 * to a set of already optimized items. Here the term {@code table[i][j]} represents the maximum cost when
 * choosing the first {@code i} items in a way that the total weight is less then {@code j}.
 * <p> If the weight of ith-item is greater than the limit j, then we can't add this item to the group. So:
 * <pre>
 *     If w<sub>i</sub> > j then table[i][j] = table[i - 1][j]
 * </pre>
 * Otherwise, we should compare the cost of adding and not adding this item and choose the one with greater
 * cost:
 * <pre>
 *     table[i][j] = max(table[i - 1][j], table[i - 1][j - w<sub>i</sub>] + cost<sub>i</sub>)
 * </pre>
 *
 * <h2>Time and Space Complexities</h2>
 * Since Dynamic Programming algorithms are exploiting memory-time trade-offs, we have to analyze this
 * algorithm from both memory and time perspectives.
 * Before going any further, we should mention that the original 0-1 Knapsack problem was designed around
 * integral weights. Since all weights are decimal numbers here, we have to first multiply each weight by
 * a large enough power of ten to make all weights integral numbers.
 * The most memory consuming part of this algorithm is the memoized table. Since this is a two dimensional
 * array, our memory complexity is:
 * <pre>
 *     If we multiply each weight by 10<sup>d</sup>:
 *     O(10<sup>d</sup>nW)
 *
 *     For small enough d values, this is equivalent to:
 *     O(nW)
 * </pre>
 * The most time-consuming part of this algorithm is related to the nested for loops. So by the same logic,
 * our time complexity is also O(nW).
 *
 * <h2>Singleton Instance</h2>
 * The current implementation of the {@link ZeroOrOnePackageOptimizer} is totally stateless and thread-safe.
 * Also, it does not make sense to create a separate instance per usage. Therefore, this implementation is
 * a singleton using the single instance enum trick.
 *
 * @author Ali Dehghani
 */
public enum ZeroOrOnePackageOptimizer implements PackageOptimizer {

    /**
     * The singleton instance of the 0-1 Knapsack implantation.
     */
    INSTANCE;

    /**
     * A bottom-up dynamic programming based implementation to solve the 0-1 Knapsack problem. Before even we start
     * the actual process, we discard the items that are heavier than the weight limit to save some time.
     *
     * <p>Since in its traditional definition, the 0-1 Knapsack weight are integers, we have to first convert the decimal
     * weights to integral ones in order to use the classical solution. Here we assumed that the given weights have at
     * most 2 decimal places. Therefore we multiply all weights to 100 and cast them to integers. With this assumption,
     * the time and space complexity of the algorithm would be:
     * <pre>
     *     O(100nW) = O(nW)
     * </pre>
     * Here we're going to stick with 2 decimal places but it's also possible to scan all weights and multiply the
     * weights with the maximum possible power of ten to avoid wrong answers due to lost precisions.
     *
     * <p> After adjusting weights, we initialize a table to store the solutions to our sub-problems. Then following
     * a typical bottom-up approach to solve the smaller problems until solving the actual problem.
     *
     * @param items         Collection of items to pick from.
     * @param maximumWeight The maximum possible weight that the Knapsack can hold.
     * @return The selected set of items, or an empty {@link Optional} when there is none.
     */
    @Override
    public Optional<SelectedItems> optimize(List<Item> items, double maximumWeight) {
        if (items == null || items.isEmpty()) return Optional.empty();
        items = discardTooHeavyItems(items, maximumWeight); // O(n)

        int precision = 100; // 10 to the power of 2. That is, at most 2 decimal places.
        int[] weights = items.stream().mapToInt(i -> (int) (i.getWeight() * precision)).toArray(); // O(n)
        int w = (int) (maximumWeight * precision);
        int n = items.size();

        SubSolution[][] table = initializeMemoizedSolutions(n, w); // O(nW)
        for (int i = 1; i < n + 1; i++) {
            for (int j = 1; j < w + 1; j++) {
                solveAndMemoizeThisSubProblem(items, weights, table, i, j); // O(nW)
            }
        }

        return collectAndReturnTheAnswer(table[n][w]);
    }

    /**
     * Since there is no point in evaluating items that are heavier than the weight limit, we're
     * going to drop them before actually processing them.
     * Also, since the caller may pass an unmodifiable collection, here we're allocating a totally
     * new collection based on the given collection.
     *
     * @param items         The items to search.
     * @param maximumWeight The weight limit.
     * @return Collection of items that are lighter than the weight limit.
     */
    private List<Item> discardTooHeavyItems(List<Item> items, double maximumWeight) {
        items = new ArrayList<>(items);
        items.removeIf(i -> i.getWeight() > maximumWeight);

        return items;
    }

    /**
     * Creates and initializes a (n + 1) * (W + 1) two dimensional array to hold sub-problem solutions.
     * All values from the first column and the first row are dummy and sentinel nodes and are filled
     * with zero values. Other nodes are also initialized with zero values but will change in near future.
     *
     * @param n The number of items.
     * @param w The weight limit.
     * @return The to-be-memoized table.
     */
    private SubSolution[][] initializeMemoizedSolutions(int n, int w) {
        SubSolution[][] table = new SubSolution[n + 1][w + 1];
        for (int i = 0; i < n + 1; i++) {
            for (int j = 0; j < w + 1; j++) {
                table[i][j] = new SubSolution();
            }
        }

        return table;
    }

    /**
     * Tries to solve a sub-problem. That is, it's going to calculate the the {@code table[i][j]} entry. The
     * {@code table[i][j]} represents the maximum possible cost when choosing between the first i items while
     * keeping the aggregated weight less than or equal to j.
     * <p>
     * Here's how we're going to solve this sub-problem:
     * <pre>
     *     If the weight of i-th item is bigger than the weight limit j, then we should exclude the i-th item. Hence
     *     it's safe to say:
     *         table[i][j] = table[i - 1][j]
     *     Otherwise, we should somehow decide whether or not to include the i-th element. In order to do so,
     *     we should compare the cost of not picking the i-th element:
     *         table[i - 1][j].cost
     *     with the cost of picking the i-th element:
     *         table[i - 1][j - w<sub>i</sub>] + cost<sub>i</sub>
     *
     *     And choose one with the greater cost. If two options are having the same cost, then we should
     *     choose the one with lower weight. In our bottom-up approach, if each table[i][j] guarantees that
     *     they always choose the lighter packages in case of identical costs, then we can be sure that the
     *     the table[n][w] (actual solution) also guarantees the same thing.
     * </pre>
     *
     * @param items           Collection of all items to choose from.
     * @param adjustedWeights Collection of all adjusted weights. These are the weights we've multiplied with a power
     *                        of ten in order to make them integers (To be compatible with traditional 0-1 Knapsack
     *                        problem).
     * @param table           The memoized table.
     * @param i               The upper bound for item indices.
     * @param j               The upper bound for weight.
     */
    private void solveAndMemoizeThisSubProblem(List<Item> items, int[] adjustedWeights, SubSolution[][] table, int i, int j) {
        int adjustedWeight = adjustedWeights[i - 1];
        double cost = items.get(i - 1).getCost();
        double weight = items.get(i - 1).getWeight();

        if (adjustedWeight > j) {
            excludeI(table, i, j);
        } else {
            double costOfExcludingI = table[i - 1][j].cost;
            double costOfIncludingI = table[i - 1][j - adjustedWeight].cost + cost;

            if (costOfExcludingI > costOfIncludingI) {
                excludeI(table, i, j);
            } else if (costOfExcludingI < costOfIncludingI) {
                includeI(items, table, i, j, adjustedWeight, costOfIncludingI);
            } else {
                // tie costs, choose lighter combinations
                double weightOfExcludingI = table[i - 1][j].getWeight();
                double weightOfIncludingI = table[i - 1][j - adjustedWeight].getWeight() + weight;

                if (weightOfExcludingI <= weightOfIncludingI) {
                    excludeI(table, i, j);
                } else {
                    includeI(items, table, i, j, adjustedWeight, costOfIncludingI);
                }
            }
        }
    }

    /**
     * This is going to include the i-th element into set of chosen elements from the first i items that are collectively
     * weigh less than the j.
     *
     * @param items  All items to select from.
     * @param table  The table to store the sub-solutions.
     * @param i      The current item index.
     * @param j      The current weight limit.
     * @param weight The adjusted weight for the i-th item.
     * @param cost   The total cost after including the i-th item.
     */
    private void includeI(List<Item> items, SubSolution[][] table, int i, int j, int weight, double cost) {
        table[i][j].cost = cost;
        table[i][j].items = new HashSet<>(table[i - 1][j - weight].items);
        table[i][j].items.add(items.get(i - 1));
    }

    /**
     * This is going to exclude the i-th element from a set of chosen elements from the first i items that are collectively
     * weigh less than the j.
     *
     * @param table The table to store the sub-solutions.
     * @param i     The current item index.
     * @param j     The current weight limit.
     */
    private void excludeI(SubSolution[][] table, int i, int j) {
        table[i][j] = table[i - 1][j];
    }

    /**
     * The {@code table[i][j]} is equal to maximum possible cost when choosing the first i items
     * weighing at most j. Therefore, the {@code table[n][w]} is the answer we were looking for
     * all day long: Choosing between n items
     *
     * @param answer The last row and column of the table representing the answer to the whole problem.
     * @return Optionally contains a collection of selected items. When there is no answer, the response
     * would be empty.
     */
    private Optional<SelectedItems> collectAndReturnTheAnswer(SubSolution answer) {
        if (answer.items.isEmpty()) return Optional.empty();
        return Optional.of(new SelectedItems(answer.items.stream().collect(toMap(i -> i, v -> 1.0))));
    }

    /**
     * A simple utility class to maintain temporary information about the optimized solution for a sub-problem.
     * For each sub-problem:
     * <ul>
     *     <li>{@link #items} represents a collection of items that are part of the solution for
     *     this particular sub-problem.</li>
     *     <li>{@link #cost} represents the maximized cost of this sub-problem.</li>
     * </ul>
     */
    private static class SubSolution {

        /**
         * Represents the chosen items for a particular sub-problem.
         */
        private Set<Item> items = new HashSet<>();

        /**
         * Represents the cost of this particular sub-problem.
         */
        private double cost = 0;

        /**
         * @return The weight of this sub-problem.
         */
        double getWeight() {
            return items.stream().mapToDouble(Item::getWeight).sum();
        }
    }
}
