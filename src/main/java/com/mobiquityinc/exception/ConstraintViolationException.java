package com.mobiquityinc.exception;

/**
 * Raised when we somehow violate a constraint.
 *
 * @author Ali Dehghani
 */
public class ConstraintViolationException extends APIException {

    /**
     * Construct a instance of this exception with just a message.
     *
     * @param message The message.
     */
    public ConstraintViolationException(String message) {
        super(message);
    }
}
